package com.dotin.model;

import java.time.Instant;

public class TransactionStartData {

    private String amount;
    private String trx;
    private String user;
    private Instant instant;
    private String threadId;

    public TransactionStartData(String amount,
                                String trx,
                                String user,
                                Instant instant,
                                String threadId) {
        this.amount = amount;
        this.trx = trx;
        this.user = user;
        this.instant = instant;
        this.threadId = threadId;
    }

    public String getAmount() {
        return amount;
    }

    public String getTrx() {
        return trx;
    }

    public String getUser() {
        return user;
    }

    public Instant getInstant() {
        return instant;
    }

    public String getThreadId() {
        return threadId;
    }

    @Override
    public String toString() {
        return "TransactionStartData{" +
                "amount='" + amount + '\'' +
                ", trx='" + trx + '\'' +
                ", user='" + user + '\'' +
                ", startInstant=" + instant +
                ", threadId='" + threadId + '\'' +
                '}';
    }
}
