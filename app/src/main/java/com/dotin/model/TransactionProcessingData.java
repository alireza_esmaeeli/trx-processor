package com.dotin.model;

import java.time.Instant;

public class TransactionProcessingData {

    private Instant instant;
    private String threadId;

    public TransactionProcessingData(Instant instant, String threadId) {
        this.instant = instant;
        this.threadId = threadId;
    }

    public Instant getInstant() {
        return instant;
    }

    public String getThreadId() {
        return threadId;
    }

    @Override
    public String toString() {
        return "TransactionProcessingData{" +
                "processingInstant=" + instant +
                ", threadId='" + threadId + '\'' +
                '}';
    }
}
