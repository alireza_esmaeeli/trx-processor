package com.dotin.model;

import java.time.Instant;

public class Transaction {

    private String amount;
    private String trx;
    private String user;
    private String code;
    private Instant startInstant;
    private Instant processingInstant;
    private Instant endInstant;

    public Transaction(TransactionStartData startData,
                       TransactionProcessingData processingData,
                       TransactionEndData endData) {
        this.amount = startData.getAmount();
        this.trx = startData.getTrx();
        this.user = startData.getUser();
        this.startInstant = startData.getInstant();
        this.processingInstant = processingData.getInstant();
        this.code = endData.getCode();
        this.endInstant = endData.getInstant();
    }

    public String getAmount() {
        return amount;
    }

    public String getTrx() {
        return trx;
    }

    public String getUser() {
        return user;
    }

    public String getCode() {
        return code;
    }

    public Instant getStartInstant() {
        return startInstant;
    }

    public Instant getProcessingInstant() {
        return processingInstant;
    }

    public Instant getEndInstant() {
        return endInstant;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "amount='" + amount + '\'' +
                ", trx='" + trx + '\'' +
                ", user='" + user + '\'' +
                ", code='" + code + '\'' +
                ", startInstant=" + startInstant +
                ", processingInstant=" + processingInstant +
                ", endInstant=" + endInstant +
                '}';
    }
}
