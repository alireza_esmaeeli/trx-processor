package com.dotin.model;

import java.time.Instant;

public class TransactionEndData {

    private String code;
    private Instant instant;
    private String threadId;

    public TransactionEndData(String code, Instant instant, String threadId) {
        this.code = code;
        this.instant = instant;
        this.threadId = threadId;
    }

    public String getCode() {
        return code;
    }

    public Instant getInstant() {
        return instant;
    }

    public String getThreadId() {
        return threadId;
    }

    @Override
    public String toString() {
        return "TransactionEndData{" +
                "code='" + code + '\'' +
                ", endInstant=" + instant +
                ", threadId='" + threadId + '\'' +
                '}';
    }
}

