package com.dotin;

import com.dotin.model.Transaction;
import com.dotin.model.TransactionEndData;
import com.dotin.model.TransactionProcessingData;
import com.dotin.model.TransactionStartData;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DecimalFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.time.ZoneOffset.*;
import static java.time.format.DateTimeFormatter.*;

public class TransactionFileProcessorApplication {

    private static final DateTimeFormatter dateTimeFormatter = ofPattern("yyyy-MM-dd HH:mm:ss,SSS");

    public static void main(String[] args) {

        Path inputPath = Path.of("src/main/resources/trx.txt");

        List<TransactionStartData> startDataList = extractStartData(inputPath);
        List<TransactionEndData> endDataList = extractEndData(inputPath);
        List<TransactionProcessingData> processingDataList = extractProcessingData(inputPath);

        List<Transaction> transactions = createTransactions(startDataList, endDataList, processingDataList);

        Path outputPath1 = Path.of("src/main/resources/result1.txt");
        writeProcessResultsToFile(transactions, outputPath1);

        Path outputPath2 = Path.of("src/main/resources/result2.txt");
        writeAveragesToFile(transactions, outputPath2);
    }

    private static List<Transaction>
    createTransactions(List<TransactionStartData> startDataList,
                       List<TransactionEndData> endDataList,
                       List<TransactionProcessingData> processingDataList) {

        return startDataList.stream()
                .map(startData ->
                {
                    var matchingEndData = endDataList.stream()
                            .filter(endData ->
                                    endData.getThreadId().equals(startData.getThreadId()))
                            .findFirst()
                            .get();
                    endDataList.remove(matchingEndData);

                    var matchingProcessingData = processingDataList.stream()
                            .filter(endData ->
                                    endData.getThreadId().equals(startData.getThreadId()))
                            .findFirst()
                            .get();
                    processingDataList.remove(matchingProcessingData);

                    return new Transaction(startData, matchingProcessingData, matchingEndData);
                })
                .toList();
    }

    private static void writeProcessResultsToFile(List<Transaction> transactions, Path path) {
        StringBuilder builder = new StringBuilder();
        for (Transaction transaction : transactions) {
            builder.append(transaction.getUser())
                    .append(",")
                    .append(transaction.getTrx())
                    .append(",")
                    .append(transaction.getCode())
                    .append(",")
                    .append(Duration.between(transaction.getStartInstant(), transaction.getProcessingInstant()).toMillis())
                    .append(",")
                    .append(Duration.between(transaction.getProcessingInstant(), transaction.getEndInstant()).toMillis())
                    .append(System.lineSeparator());
        }

        writeToFile(path, builder);
    }

    private static void writeAveragesToFile(List<Transaction> transactions, Path path) {
        StringBuilder builder = new StringBuilder();
        List<String> types = List.of("balance", "bill", "charge");

        for (String type : types) {
            builder.append(type)
                    .append(",")
                    .append(formatMilliseconds(calculateAverageBetweenStartAndProcessing(transactions, type)))
                    .append(",")
                    .append(formatMilliseconds(calculateAverageBetweenProcessingAndEnd(transactions, type)))
                    .append(System.lineSeparator());
        }

        writeToFile(path, builder);
    }

    private static void writeToFile(Path path, StringBuilder builder) {
        try {
            Files.writeString(path, builder.toString());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    private static double calculateAverageBetweenProcessingAndEnd(List<Transaction> transactions, String type) {
        return transactions.stream()
                .filter(transaction ->
                        transaction.getTrx().equals(type))
                .mapToLong(transaction ->
                        Duration.between(transaction.getProcessingInstant(), transaction.getEndInstant()).toMillis())
                .average()
                .getAsDouble();
    }

    private static double calculateAverageBetweenStartAndProcessing(List<Transaction> transactions, String type) {
        return transactions.stream()
                .filter(transaction ->
                        transaction.getTrx().equals(type))
                .mapToLong(transaction ->
                        Duration.between(transaction.getStartInstant(), transaction.getProcessingInstant()).toMillis())
                .average()
                .getAsDouble();
    }

    private static List<TransactionStartData> extractStartData(Path path) {
        List<TransactionStartData> startDataList = new ArrayList<>();
        try (Stream<String> lines = Files.lines(path)) {
            startDataList =
                    lines.filter(line -> line.contains("starting"))
                            .flatMap(TransactionFileProcessorApplication::lineToTransactionStartData)
                            .collect(Collectors.toList());

        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return startDataList;
    }

    private static List<TransactionEndData> extractEndData(Path path) {
        List<TransactionEndData> endDataList = new ArrayList<>();
        try (Stream<String> lines = Files.lines(path)) {
            endDataList =
                    lines.filter(line -> line.contains("end"))
                            .flatMap(TransactionFileProcessorApplication::lineToTransactionEndData)
                            .collect(Collectors.toList());

        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return endDataList;
    }

    private static List<TransactionProcessingData> extractProcessingData(Path path) {
        List<TransactionProcessingData> processingDataList = new ArrayList<>();
        try (Stream<String> lines = Files.lines(path)) {
            processingDataList =
                    lines.filter(line -> line.contains("processing"))
                            .flatMap(TransactionFileProcessorApplication::lineToTransactionProcessingData)
                            .collect(Collectors.toList());

        } catch (IOException exception) {
            exception.printStackTrace();
        }
        return processingDataList;
    }

    private static Stream<TransactionStartData> lineToTransactionStartData(String line) {

        List<String> fields =
                Arrays.stream(line.transform(l -> l.substring(l.indexOf("{") + 1, l.indexOf("}")))
                        .split(",")).toList();

        Map<String, String> map = new HashMap<>();

        fields.forEach(field -> {
            String[] tokens = field.split(":");
            map.put(tokens[0].strip().replace("\"", ""), tokens[1].strip().replace("\"", ""));
        });

        return Stream.of(new TransactionStartData(
                map.get("amount"),
                map.get("trx"),
                map.get("user"),
                extractDateTime(line),
                extractThreadId(line)
        ));
    }

    private static Stream<TransactionEndData> lineToTransactionEndData(String line) {

        List<String> fields =
                Arrays.stream(line.transform(l -> l.substring(l.indexOf("{") + 1, l.indexOf("}")))
                        .split(",")).toList();

        Map<String, String> map = new HashMap<>();

        fields.forEach(field -> {
            String[] tokens = field.split("=");
            map.put(tokens[0].strip().replace("\"", ""), tokens[1].strip().replace("\"", ""));
        });

        return Stream.of(new TransactionEndData(
                map.get("code"),
                extractDateTime(line),
                extractThreadId(line)
        ));
    }

    private static Stream<TransactionProcessingData> lineToTransactionProcessingData(String line) {

        return Stream.of(new TransactionProcessingData(
                extractDateTime(line),
                extractThreadId(line)
        ));
    }

    private static Instant extractDateTime(String line) {
        String[] split = line.split("\\[thread-\\d+\\]");
        return LocalDateTime.parse(split[0].strip(), dateTimeFormatter).toInstant(UTC);
    }

    private static String extractThreadId(String line) {
        return line.transform(l -> l.substring(l.indexOf("[") + 1, l.indexOf("]"))).strip();
    }

    private static String formatMilliseconds(double milliseconds) {
        DecimalFormat formatter = new DecimalFormat("###");
        return formatter.format(milliseconds);
    }
}
